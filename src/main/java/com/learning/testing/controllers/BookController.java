package com.learning.testing.controllers;

import com.learning.testing.models.Book;
import com.learning.testing.services.BookService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/v1/books")
public class BookController {


    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping
    public List<Book> getAllBooks() {
        return bookService.getAllBooks();
    }

    @PostMapping
    public void addStudent( @RequestBody Book book) throws IllegalArgumentException {
        bookService.saveBook(book);
    }

    @DeleteMapping(path = "{bookId}")
    public void deleteBook(
            @PathVariable("bookId") Long bookId) {
        bookService.deleteBook(bookId);
    }

}
