package com.learning.testing.repositories;

import com.learning.testing.models.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends JpaRepository< Book, Long> {

    @Query("" +
            "SELECT CASE WHEN COUNT(bk) > 0 THEN " +
            "TRUE ELSE FALSE END " +
            "FROM Book bk " +
            "WHERE bk.title = ?1"
    )
    Boolean selectExistsTitle(String title);
}
