package com.learning.testing.services;


import com.learning.testing.exceptions.BadRequestException;
import com.learning.testing.exceptions.BookNotFoundException;
import com.learning.testing.models.Book;
import com.learning.testing.repositories.BookRepository;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;

@Service
public class BookService {

    private final BookRepository bookRepository;

    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public Book saveBook(Book book)  {
        boolean exists= bookRepository.selectExistsTitle(book.getTitle());

        if(exists)
        {
            throw new BadRequestException("Book already exist");
        }

        return this.bookRepository.save(book);
    }

    public List<Book> getAllBooks(){

        return this.bookRepository.findAll();
    }

    public Optional<Book> getBookById(Long id)
    {


        return this.bookRepository.findById(id);
    }
    public void deleteBook(Long id)
    {
        boolean exist= bookRepository.existsById(id);

        if(!exist)
        {
          throw new BookNotFoundException("Book does not Exist");
        }

        bookRepository.deleteById(id);
    }
}
