package com.learning.testing.services;

import com.learning.testing.exceptions.BadRequestException;
import com.learning.testing.exceptions.BookNotFoundException;
import com.learning.testing.models.Book;
import com.learning.testing.repositories.BookRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class BookServiceTest {
   @Mock private BookRepository bookRepository;
   private BookService bookService;

    @BeforeEach
    void setUp() {
        bookService= new BookService(bookRepository);
    }

    @Test
    void canSaveBook() {
        Book book = new Book(10L,"Java Simple", "Tinotenda Ruzane");

        bookService.saveBook(book);

        ArgumentCaptor<Book> bookArgumentCaptor= ArgumentCaptor.forClass(Book.class);
        verify(bookRepository).save(bookArgumentCaptor.capture());

        Book capturedBook = bookArgumentCaptor.getValue();
        assertThat(capturedBook).isEqualTo(book);
    }

    @Test
    void canNotSaveBook() {
        Book book = new Book(10L,"Java Simple", "Tinotenda Ruzane");

        given(bookRepository.selectExistsTitle(any())).willReturn(true);

        assertThatThrownBy(()-> bookService.saveBook(book))
                .isInstanceOf(BadRequestException.class)
                .hasMessageContaining("Book already exist");

        verify(bookRepository,never()).save(book);
    }

    @Test
    void canGetBookById() {

        long id= 10;
        bookService.getBookById(id);

        verify(bookRepository).findById(id);
    }

    @Test
    void canGetAllBooks() {
        //when
        bookService.getAllBooks();


        verify(bookRepository).findAll();

    }

    @Test
    void canDeleteBookById() {
   long id=10;

   given(bookRepository.existsById(id))
           .willReturn(true);
bookService.deleteBook(id);
   verify(bookRepository).deleteById(id);

    }

    @Test
    void canNotDeleteBookById() {
        long id=10;

        given(bookRepository.existsById(id))
                .willReturn(false);

        assertThatThrownBy(()-> bookService.deleteBook(id))
                .isInstanceOf(BookNotFoundException.class)
                .hasMessageContaining("Book does not Exist");

        verify(bookRepository, never()).deleteById(id);

    }
}