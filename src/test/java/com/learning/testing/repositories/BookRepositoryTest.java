package com.learning.testing.repositories;

import com.learning.testing.models.Book;
import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.assertj.core.api.Assertions.assertThat;


@DataJpaTest
class BookRepositoryTest {

   @Autowired
    BookRepository bookRepository;

   @AfterEach
   void tearDown(){bookRepository.deleteAll();}

   @Test
    void itShouldCheckIfBookExistsByTitle(){
       //given
    String title ="Java for Dummies";

       Book book  = new Book(1L,title, "John Doe");
       bookRepository.save(book);

       //when
      boolean expected= bookRepository.selectExistsTitle(title);

      //then
      assertThat(expected).isTrue();
   }

    @Test
    void itShouldCheckWhenBookTitleDoesNotExists() {
        // given
        String title = "Java Data Structures";

        // when
        boolean expected = bookRepository.selectExistsTitle(title);

        // then
        AssertionsForClassTypes.assertThat(expected).isFalse();
    }

}